﻿#include <iostream>

template<class T>
class UniquePointer {
private:
    T* ptr;

public:

    UniquePointer(T* p) : ptr(p) {}

    ~UniquePointer() {
        delete ptr;
    }

    UniquePointer(UniquePointer&& other) noexcept : ptr(other.ptr) {
        other.ptr = nullptr;
    }

    UniquePointer& operator=(UniquePointer&& other) noexcept {
        if (this != &other)
        {
            delete ptr;
            ptr = other.ptr;
            other.ptr = nullptr;
        }
        return *this;
    }

    UniquePointer(const UniquePointer& other) = delete;
    UniquePointer& operator=(const UniquePointer& other) = delete;

    T& operator*() const {
        return *ptr;
    }

    T* operator->() const {
        return ptr;
    }

};



int main()
{
    UniquePointer<int> uptr(new int(42));
    std::cout << "The value pointed to by the UniquePointer: " << *uptr << "\n";

    UniquePointer<int> uptr2 = std::move(uptr);
    std::cout << "The value after the move: " << *uptr2 << "\n";

    return 0;
}
